<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApiIntegrationTest extends TestCase
{
    use DatabaseTransactions;

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        print "\nLoading the json fixture from the data directory (GB.json)\n";
        //Loads the GB.json file from the /tests/data directory using the DATA_DIR environment variable from phpunit.xml
        shell_exec('php artisan migrate');
        shell_exec('php artisan import:json --force');
    }

    public function testCanWeReturnAllHolidays()
    {
        $response = $this->json('get', '/v1/holidays',
            [
                'country' => 'GB',
                'year' => '2016',

            ])
            ->seeJsonStructure([
                'holidays'
            ])
            ->assertResponseStatus('200')->response;

        $json = json_decode($response->getContent(), true);
        $this->assertSame(12, count($json['holidays']));
    }

    public function testWeCanReturnASpecificMonthsHolidays()
    {
        $response = $this->json('get', '/v1/holidays',
            [
                'country' => 'GB',
                'year' => '2016',
                'month' => 12
            ])
            ->seeJsonStructure([
                'holidays'
            ])
            ->assertResponseStatus('200')->response;

        $json = json_decode($response->getContent(), true);
        $this->assertSame(2, count($json['holidays']));
    }


    public function testWeCanReturnASpecificDatesHolidays()
    {
        $response = $this->json('get', '/v1/holidays',
            [
                'country' => 'GB',
                'year' => '2016',
                'day' => 1,
                'month' => 1
            ])
            ->seeJsonStructure([
                'holidays'
            ])
            ->assertResponseStatus('200')->response;

        $json = json_decode($response->getContent(), true);
        $this->assertSame("2016-01-01", $json['holidays'][0]['date']);
    }


    public function testWeCanReturnPreviousHolidays()
    {
        $response = $this->json('get', '/v1/holidays',
            [
                'country' => 'GB',
                'year' => '2016',
                'day' => 1,
                'month' => 1,
                'previous' => true
            ])
            ->seeJsonStructure([
                'holidays'
            ])
            ->assertResponseStatus('200')->response;

        $json = json_decode($response->getContent(), true);
        $this->assertSame('2015-12-28', $json['holidays'][0]['date']);
    }

    public function testWeCanReturnUpcomingHolidays()
    {
        $response = $this->json('get', '/v1/holidays',
            [
                'country' => 'GB',
                'year' => '2016',
                'day' => 1,
                'month' => 1,
                'upcoming' => true
            ])
            ->seeJsonStructure([
                'holidays'
            ])
            ->assertResponseStatus('200')->response;

        $json = json_decode($response->getContent(), true);
        $this->assertSame('2016-02-14', $json['holidays'][0]['date']);
    }


    public function testWeCanReturnPublicHolidays()
    {
        $response = $this->json('get', '/v1/holidays',
            [
                'country' => 'GB',
                'year' => '2016',
                'public' => true
            ])
            ->seeJsonStructure([
                'holidays'
            ])
            ->assertResponseStatus('200')->response;

        $json = json_decode($response->getContent(), true);
        $this->assertSame("2016-01-01", $json['holidays']['2016-01-01'][0]['date']);
    }


}
