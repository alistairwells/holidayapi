<?php

use App\Api;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class ApiUnitTest
 *
 * Unit test that doesn't touch the database
 *
 * @var $api Api
 */
class ApiUnitTest extends TestCase
{
    public $api;

    public function setUp()
    {
        $this->api = new Api();
    }

    public function tearDown()
    {
        unset($this->api);
    }

    //Shortcuts we to see we have an error and a 400 response
    public function seeErrors($data)
    {
        $this->api->validateInput($data);
        $this->assertArrayHasKey('errors', $this->api->results());
        $this->assertSame($this->api->status, 400);
    }

    //Shortcuts we to see we have an holidays array (empty or otherwise)
    public function seeHolidays($data)
    {
        $this->api->validateInput($data);
        $this->assertArrayHasKey('holidays', $this->api->results());
        $this->assertSame($this->api->status, 200);
    }

    //Loop through the data to make sure it matches what was passed via the request
    public function matchProperties($data)
    {
        foreach($data as $k => $v){
            $this->assertSame($this->api->$k, $v);
        }
    }

    public function testWeReturnErrorWhenNoDataPassed()
    {
        $data = [];
        $this->seeErrors($data);
    }

    public function testWeReturnErrorWhenNoCountryPassed()
    {
        $data = ['year' => 2016];
        $this->seeErrors($data);
    }

    public function testWeReturnErrorWhenNoYearPassed()
    {
        $data = ['country' => 'US'];
        $this->seeErrors($data);
    }


    public function testWeReturnErrorWhenTooSmallYearPassed()
    {
        $data = [
            'country' => 'US',
            'year' => '99'
        ];
        $this->seeErrors($data);
    }

    public function testWeReturnErrorWhenTooLargeYearPassed()
    {
        $data = [
            'country' => 'US',
            'year' => '997667',
        ];
        $this->seeErrors($data);

    }

    public function testWeReturnErrorWhenNonNumericYearPassed()
    {
        $data = [
            'country' => 'US',
            'year' => 'test',
        ];
        $this->seeErrors($data);

    }

    public function testWeReturnErrorWhenInvalidCountryPassed()
    {
        $data = [
            'country' => 'USA',
            'year' => '2016',

        ];
        $this->seeErrors($data);
    }

    public function testWeReturnErrorWhenTooSmallMonthPassed()
    {
        $data = [
            'country' => 'XX',
            'year' => '2000',
            'month' => 0
        ];
        $this->seeErrors($data);
    }

    public function testWeReturnErrorWhenTooLargeMonthPassed()
    {
        $data = [
            'country' => 'XX',
            'year' => '2000',
            'month' => 13
        ];
        $this->seeErrors($data);
    }

    public function testWeReturnErrorWhenTooSmallDayPassed()
    {
        $data = [
            'country' => 'XX',
            'year' => '2000',
            'day' => 0
        ];
        $this->seeErrors($data);
    }

    public function testWeReturnErrorWhenTooLargeDayPassed()
    {
        $data = [
            'country' => 'XX',
            'year' => '2000',
            'day' => '32'
        ];
        $this->seeErrors($data);
    }

    public function testStrictModeReturnsErrorOnInvalidDate()
    {

        $this->api->strict_mode_enabled = true;

        //Impossible date 31st of Feb
        $data = [
            'country' => 'XX',
            'year' => '2016',
            'month' => '02',
            'day' => '31'
        ];

        $this->seeErrors($data);
    }


    public function testUpcomingRequiresMonth()
    {
        $data = [
            'country' => 'XX',
            'year' => '2016',
            'upcoming' => '',
            'day' => '01'
        ];

        $this->seeErrors($data);
    }

    public function testUpcomingRequiresDay()
    {
        $data = [
            'country' => 'XX',
            'year' => '2016',
            'upcoming' => '',
            'month' => '01'
        ];

        $this->seeErrors($data);
    }

    public function testPreviousRequiresMonth()
    {
        $data = [
            'country' => 'XX',
            'year' => '2016',
            'previous' => '',
            'day' => '01'
        ];

        $this->seeErrors($data);
    }

    public function testPreviousRequiresDay()
    {

        //Impossible date
        $data = [
            'country' => 'XX',
            'year' => '2016',
            'upcoming' => true,
            'month' => '01'
        ];

        $this->seeErrors($data);
    }

    public function testCantHaveBothPreviousAndUpcoming()
    {
        $data = [
            'country' => 'XX',
            'year' => '2016',
            'previous' => true,
            'upcoming' => true,
            'month' => 01,
            'day' => 01
        ];

        $this->seeErrors($data);
    }


    public function testALeapYearIsValid()
    {

        $this->api->strict_mode_enabled = true;

        $data = [
            'country' => 'XX',
            'year' => '2016',
            'month' => 02,
            'day' => 29
        ];

        $this->seeHolidays($data);
    }


    public function testWeCanPassCountryAndYear()
    {
        $data = [
            'country' => 'XX',
            'year' => '2000',

        ];
        $this->seeHolidays($data);
    }

    public function testWeCanPassCountryYearAndMonth()
    {
        $data = [
            'country' => 'XX',
            'year' => '2000',
            'month' => 01

        ];
        $this->seeHolidays($data);
    }

    public function testWeCanPassCountryYearMonthAndDay()
    {
        $data = [
            'country' => 'XX',
            'year' => '2000',
            'month' => '11',
            'day' => '11'

        ];
        $this->seeHolidays($data);
        $this->matchProperties($data);
    }

    public function testWeCanPassPrevious()
    {
        $data = [
            'country' => 'XX',
            'year' => '2000',
            'month' => '11',
            'day' => '11',
            'previous' => true

        ];
        $this->seeHolidays($data);
        $this->matchProperties($data);
    }

    public function testWeCanPassUpcoming()
    {
        $data = [
            'country' => 'XX',
            'year' => '2000',
            'month' => '11',
            'day' => '11',
            'upcoming' => true

        ];
        $this->seeHolidays($data);
        $this->matchProperties($data);
    }

    public function testWeCanPassPublic()
    {
        $data = [
            'country' => 'XX',
            'year' => '2000',
            'public' => true

        ];
        $this->seeHolidays($data);
        $this->matchProperties($data);
    }

    public function testResultsAlwaysReturnsArray()
    {
        $this->assertArrayHasKey('status', $this->api->results());
    }

    //Wanted to mock the Cache here for testing but didn't have time
}
