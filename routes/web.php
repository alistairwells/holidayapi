<?php

//Return the home view, in a larger site this would be offset to it's own controller action
Route::get('/', function () {
    return view('home');
});
