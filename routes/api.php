<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/v1/holidays', 'HolidayApiController@v1');

//Send 404 for any HTTP request that doesn't match the API endpoint above
Route::any('{any}', function(){
    return response('', 404);
});
