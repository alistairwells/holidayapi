# Croud Coding Test

### Installation

I'm guessing you'll have [Composer](https://getcomposer.org/doc/00-intro.md) installed and ready to rock. I developed this on [Homestead VM instance](https://laravel.com/docs/5.3/homestead)

Please make two databases with UTF-8 encoding. I have called them holidayapi and holidayapi_testing but that can be changed in the .env file and the phpunit.xml configuration.

`git clone https://alistairwells@bitbucket.org/alistairwells/holidayapi.git`

`cd holidayapi`

`composer install`

`php artisan migrate`

`php artisan import:json`

Now set up the .env for the project, I've copied my example to .env.example 

### JSON Import Command

This can be ran from the project route with

`php artisan import:json`

I've included the ConfirmableTrait so it requires a yes/no when the APP_ENV is production or the --force option. I've also added a --truncate option to reset the primary keys

### Public Holidays Flag

I added is_public_holiday to the holiday table and a few basic parses to best-guess the holidays.

### Public Holidays API Parameter

I added a parameter called public to restrict the query to public holidays only.

### API Rebuild

I've switched the API to use the database instead of the JSON files. It uses the build in Validator and Cache driver of Laravel and I added a few extra bits of validation too. 

There were a few more bits I wanted to add/re-factor but didn't want to deviate from the spec too much; maybe we can talk through them if I get the interview 😉

### Response Codes

We are returning 200 on success and 400 on error. Any non GET requests should result in a 404 as defined in the api.php routes file.

### Test Suite

I wrote a pure unit test that checks the validation logic from the API. I've also wrote an integration test that  the GB.json as data fixture to simulate actual HTTP requests to the API.

You more than likely going to need need to configure the $baseUrl property in TestCase.php to get it to run.