<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'country';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function holidays()
    {
        return $this->hasMany(Holiday::class, 'country_id');
    }
}
