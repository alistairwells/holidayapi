<?php

namespace App;

use Cache;
use Carbon\Carbon;
use Exception;
use Validator;

class Api
{
    public $country;
    public $year;
    public $month = '';
    public $day = '';
    public $date;
    public $previous = false;
    public $upcoming = false;
    public $public = false;
    public $status = 200;

    public $cache = false;
    public $cache_key;
    public $errors = [];
    public $holidays = [];
    // true turns on some extra validation logic, set to false to revert to the logic from before
    public $strict_mode = true;


    /**
     * Api constructor.
     * @param string $cache
     */
    public function __construct($cache = '')
    {
        $this->cache = $cache;

        //See we can access the cache
        try {
            if ($this->cache) {
                //Increment the cache
                Cache::store($this->cache)->increment('holidayapi:requests:alltime');
            }
        } catch (Exception $e) {
            //Could force-fail the entire request by an entry adding to the error log but just going to disable the cache instead
            //$this->errors[] = ($e->getMessage());
            $this->cache = false;
        }

    }


    /**
     * Validates the input from the HTTP request
     *
     * @param array $input
     */
    public function validateInput(array $input)
    {
        $validator = Validator::make($input, [
            'country' => 'required|size:2|string',
            'year' => 'required|digits:4',
            'month' => 'numeric|between:1,12',
            'day' => 'numeric|between:1,31',

        ]);

        //Push these errors to the errors array here to retain the ordering
        $this->errors = array_merge($this->errors, collect($validator->errors()->toArray())->flatten()->toArray());

        $this->month = isset($input['month']) ? str_pad($input['month'], 2, '0', STR_PAD_LEFT) : '';
        $this->day = isset($input['day']) ? str_pad($input['day'], 2, '0', STR_PAD_LEFT) : '';

        $this->previous = isset($input['previous']);
        $this->upcoming = isset($input['upcoming']);
        $this->public = isset($input['public']);

        if ($this->previous && $this->upcoming) {
            $this->errors[] = 'You cannot request both previous and upcoming holidays.';
        } elseif (($this->previous || $this->upcoming) && (!$this->month || !$this->day)) {
            $request = $this->previous ? 'previous' : 'upcoming';
            $missing = !$this->month ? 'month' : 'day';

            $this->errors[] = 'The ' . $missing . ' parameter is required when requesting ' . $request . ' holidays.';
        }

        //Passed the first phase of validation, now perform more advanced date validation
        if (count($this->errors) === 0) {
            $this->country = $input['country'];
            $this->year = $input['year'];
            $this->cache_key = 'holidayapi:' . $this->country . ':holidays:' . $this->year;
            $this->date = $this->year . '-' . $this->month . '-' . $this->day;

            // Changed this a bit this around to ensure that dates are truly valid
            // Before 30th Feb would resolve to 1st or 2nd of March depending if it was a leap year
            // Set $this->strict_mode to false to revert to previous validation logic
            if ($this->month && $this->day && $this->strict_mode) {

                $date = Carbon::createFromFormat('Y-m-d', $this->date);

                if ($date->format('Y-m-d') !== $this->date) {
                    $this->errors[] = 'The supplied date (' . $this->date . ') is invalid.';
                }
            } elseif ($this->month && $this->day) {
                if (strtotime($this->date) === false) {
                    $this->errors[] = 'The supplied date (' . $this->date . ') is invalid.';
                }
            }
        }

        if (count($this->errors) > 0) {
            $this->status = 400;
        }
    }


    /**
     * Perform the full API operation in sequence
     *
     * @param $data
     * @return array
     */
    public function query($data)
    {
        $this->validateInput($data);

        if (count($this->errors) === 0) {
            $this->getHolidays();
        }

        return $this->results();
    }

    /**
     * Return an array of the errors or holidays
     *
     * @return array
     */
    public function results()
    {
        $payload = ['holidays' => $this->holidays];

        if (count($this->errors) > 0) {
            $payload = ['errors' => $this->errors];
        }
        $payload['status'] = $this->status;

        return $payload;
    }

    /**
     * @return array|mixed
     */
    public function getHolidays()
    {
        if (ini_get('date.timezone') == '') {
            date_default_timezone_set('UTC');
        }

        $payload = [];

        try {
            $country_holidays = $this->calculateHolidays($this->country, $this->year, $this->previous || $this->upcoming, $this->public);

            if ($this->status == 200) {

                if ($this->month && $this->day) {
                    if ($this->previous) {
                        $country_holidays = $this->flatten($this->date, $country_holidays[$this->year - 1], $country_holidays[$this->year]);
                        prev($country_holidays);
                        $payload = current($country_holidays);
                    } elseif ($this->upcoming) {
                        $country_holidays = $this->flatten($this->date, $country_holidays[$this->year], $country_holidays[$this->year + 1]);
                        next($country_holidays);
                        $payload = current($country_holidays);
                    } elseif (isset($country_holidays[$this->year][$this->date])) {
                        $payload = $country_holidays[$this->year][$this->date];
                    }
                } elseif ($this->month) {
                    foreach ($country_holidays[$this->year] as $date => $country_holiday) {
                        if (substr($date, 0, 7) == $this->year . '-' . $this->month) {
                            $payload = array_merge($payload, $country_holiday);
                        }
                    }
                } else {
                    $payload = $country_holidays[$this->year];
                }
            }

        } catch (Exception $e) {
            $this->status = 400;
            $this->errors[] = $e->getMessage();
        }

        return $this->holidays = $payload;
    }

    /**
     * Perform the database lookup and string parsing
     * @todo I'd split into smaller methods and re-factor some of the logic although out of scope for now
     *
     * @param string $county_code Country code
     * @param integer $year
     * @param bool $range
     * @param bool $public
     * @return array
     * @throws Exception
     */
    private function calculateHolidays($county_code, $year, $range = false, $public = false)
    {
        $return = [];

        if ($range) {
            $years = [$year - 1, $year, $year + 1];
        } else {
            $years = [$year];
        }

        foreach ($years as $year) {
            if ($this->cache) {

                $country_holidays = Cache::store($this->cache)->get($this->cache_key);

            } else {
                $country_holidays = false;
            }

            if ($country_holidays) {
                $country_holidays = unserialize($country_holidays);
            } else {

                $country = Country::where('code', strtoupper($county_code))->with(['holidays' => function ($q) use ($public) {
                    if ($public) {
                        $q->where('is_public_holiday', 1);
                    }
                }])->first();

                if (is_null($country)) {
                    throw new Exception('The supplied country (' . $county_code . ') is not supported at this time.');
                }

                $calculated_holidays = [];

                foreach ($country->holidays->toArray() as $holiday) {
                    if (strstr($holiday['rule'], '%Y')) {
                        $rule = str_replace('%Y', $year, $holiday['rule']);
                    } elseif (strstr($holiday['rule'], '%EASTER')) {
                        $rule = str_replace('%EASTER', date('Y-m-d', strtotime($year . '-03-21 +' . easter_days($year) . ' days')), $holiday['rule']);
                    } elseif (in_array($county_code, ['BR', 'US']) && strstr($holiday['rule'], '%ELECTION')) {
                        switch ($county_code) {
                            case 'BR':
                                $years = range(2014, $year, 2);
                                break;
                            case 'US':
                                $years = range(1788, $year, 4);
                                break;
                        }

                        if (in_array($year, $years)) {
                            $rule = str_replace('%ELECTION', $year, $holiday['rule']);
                        } else {
                            $rule = false;
                        }
                    } else {
                        $rule = $holiday['rule'] . ' ' . $year;
                    }

                    if ($rule) {
                        $calculated_date = date('Y-m-d', strtotime($rule));

                        if (!isset($calculated_holidays[$calculated_date])) {
                            $calculated_holidays[$calculated_date] = [];
                        }

                        $calculated_holidays[$calculated_date][] = [
                            'name' => $holiday['name'],
                            'country' => $county_code,
                            'date' => $calculated_date,
                            'is_public_holiday' => $holiday['is_public_holiday']
                        ];
                    }
                }


                $country_holidays = $calculated_holidays;

                ksort($country_holidays);

                foreach ($country_holidays as $date_key => $date_holidays) {
                    usort($date_holidays, function ($a, $b) {
                        $a = $a['name'];
                        $b = $b['name'];

                        if ($a == $b) {
                            return 0;
                        }

                        return $a < $b ? -1 : 1;
                    });

                    $country_holidays[$date_key] = $date_holidays;
                }

                if ($this->cache) {
                    Cache::store($this->cache)->put($this->cache_key, serialize($country_holidays), 60);
                }
            }

            $return[$year] = $country_holidays;
        }

        return $return;
    }

    private function flatten($date, $array1, $array2)
    {
        $holidays = array_merge($array1, $array2);

        // Injects the current date as a placeholder
        if (!isset($holidays[$date])) {
            $holidays[$date] = false;
            ksort($holidays);
        }

        // Sets the internal pointer to today
        while (key($holidays) !== $date) {
            next($holidays);
        }

        return $holidays;
    }
}