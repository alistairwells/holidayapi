<?php

namespace App\Console\Commands;

use App\Country;
use DB;
use Illuminate\Console\Command;
use File;
use Exception;
use Illuminate\Console\ConfirmableTrait;


class ImportJson extends Command
{
    use ConfirmableTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:json {--truncate} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import the the holiday information into the database from JSON files in the data directory';


    /**
     * Path to the JSON files; initialised with the constructor to allow the use of resource_path();
     * Can be set on a pe- instance basis by changing the DATA_DIR environment variable in .env
     *
     * @var string
     */
    private $data_dir = '';


    /**
     * Keywords for use with parseNamePublicHoliday()
     *
     * @var array
     */
    private $name_public_holiday_keywords = ['bank', 'lieu'];

    /**
     * Keywords for use with parseRulePublicHoliday()
     *
     * @var array
     */
    private $rule_public_holiday_keywords = ['%EASTER +1 day', '%EASTER -2 day', 'January 1st'];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->data_dir = env('DATA_DIR', resource_path('data'));
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (! $this->confirmToProceed()) {
            return;
        }

        try {
            if ($this->option('truncate')) {

                if ($this->confirm('This will reset the primary keys before the import, are you sure you want to do this?')) {
                    $this->truncateTables();
                }
            }

            $this->importFromJson();

        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    public function importFromJson()
    {
        $files = File::allFiles($this->data_dir);

        if (count($files) === 0) {
            throw new Exception("There are no files in the {$this->data_dir} directory");
        }

        foreach ($files as $file) {
            if (strtolower($file->getExtension() === 'json')) {

                //Decode the JSON and convert to an associative array for ease passing to createMany();
                //Presuming the JSON is invalid if null is returned
                $json = json_decode(file_get_contents($file->getRealPath()), true);
                if (is_null($json)) {
                    throw new Exception("The file name of {$file->getBasename()} doesn't seem to be valid JSON");
                }

                //Get code from filename and check it's a valid ISO 3166-1 alpha-2 format
                $code = strtoupper($file->getBasename('.' . $file->getExtension()));
                if (strlen($code) !== 2) {
                    throw new Exception("The file name of {$file->getBasename()} must be the name of the country in ISO 3166-1 alpha-2 format. http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2");
                }

                //Attempt to parse the Great British public holidays
                if ($code === 'GB') {
                    $json = array_map([$this, 'parseNamePublicHoliday'], $json);
                }

                //Attempt to parse the Easter public holidays
                $json = array_map([$this, 'parseRulePublicHoliday'], $json);


                //If this was a larger data set I'd use raw SQL instead of the Eloquent ORM methods
                //I thought it could be a nice way to show I understood the relationship concepts
                $country = Country::firstOrCreate(['code' => $code]);
                //Remove existing holidays and bulk insert the new entries from the JSON file
                $country->holidays()->delete();
                $country->holidays()->createMany($json);

                //Output what the crack is with the above entry
                $this->info("$code country created and {$country->holidays->count()} holidays inserted");
            }
        }
    }


    /**
     * Crude way of parsing the public holidays from Great Britain
     *
     * @param array $json_row
     * @return array
     */
    public function parseNamePublicHoliday(array $json_row)
    {
        foreach ($this->name_public_holiday_keywords as $keyword) {
            if (stristr($json_row['name'], $keyword) !== false) {
                $json_row['is_public_holiday'] = 1;
            }
        }

        return $json_row;
    }

    /**
     * Crude way of parsing the Easter holidays
     *
     * @param array $json_row
     * @return array
     */
    public function parseRulePublicHoliday(array $json_row)
    {

        foreach ($this->rule_public_holiday_keywords as $keyword) {
            if (stristr($json_row['rule'], $keyword) !== false) {
                $json_row['is_public_holiday'] = 1;
            }
        }

        return $json_row;
    }


    /**
     * Deletes all data from the holiday and country tables and resets their primary keys
     */
    private function truncateTables()
    {
        $this->info('Truncating the holiday and country tables');
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('holiday')->truncate();
        DB::table('country')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

}
