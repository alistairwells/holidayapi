<?php

namespace App\Http\Controllers;

use App\Http\Requests\ApiRequest;
use Dotenv\Exception\ValidationException;
use Exception;
use Illuminate\Http\Request;
use App\Api;
use Redis;
use Validator;

class HolidayApiController extends Controller
{
    public function customValidator()
    {

    }

    public function v1(Request $request)
    {
        $flags = JSON_UNESCAPED_UNICODE;

        if (isset($_REQUEST['pretty'])) {
            $flags |= JSON_PRETTY_PRINT;
        }

        if (isset($_REQUEST['pretty'])) {
            $flags |= JSON_PRETTY_PRINT;
        }

        //Switch out the cache driver at runtime
        $cache = 'file';
//        $cache = 'redis';

        $api = new Api();

        return response()->json($api->query($request->all()), $api->status, [], $flags);

    }


}