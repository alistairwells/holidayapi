<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHolidayTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('holiday', function(Blueprint $table)
		{
			$table->foreign('country_id', 'holiday_ibfk_1')->references('id')->on('country')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('holiday', function(Blueprint $table)
		{
			$table->dropForeign('holiday_ibfk_1');
		});
	}

}
