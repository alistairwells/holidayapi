<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHolidayTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('holiday', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('country_id')->unsigned()->index('country_id');
			//Left these at the default of 255 as not sure what the maximum length is
			$table->string('name')->default('');
			$table->string('rule')->default('');
			$table->boolean('is_public_holiday')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('holiday');
	}

}
